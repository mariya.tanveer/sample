defmodule EnumAndStream do
  # Most Enum functions have this signature:
  ## Enum.function(Enumerable, fun)

  # Types which implement Enumerable protocol
  ## Lists
  ## Keyword Lists
  ## Maps (not Structs)
  ## Ranges
  ## Streams

  IO.puts "Gets an element of a list at  certain index"
  IO.inspect Enum.at([1, 2, 3], 2, :default), label: "Gets an element of a list at  certain index"

  IO.puts "with maps it returns tupal, with key as first element and value is second"
  IO.inspect Enum.at(%{name: "Mariya"}, 0, :default)

  IO.puts "Returns list of values that pass the function"
  IO.inspect Enum.filter ["string", 2, {}, %{}], fn(val) ->
    is_number(val)
  end

  # map behaves as the list of tupals
  IO.inspect Enum.filter %{name: "Mariya", dob: 1991}, fn({_key, val}) ->
    is_binary(val)
  end

  IO.puts "Reduces as enumerable to a single value"
  IO.inspect Enum.reduce [1, 2, 3], 0, fn(num, sum) ->
    sum + num
  end

  IO.inspect Enum.reduce ["episodes", "07-enum-and-stream"], "", fn(segment, path) ->
    path <> "/" <> segment
  end

  IO.puts "convert an Enumerable to another type. Target must implement collectable protocol."
  %{name: "Mariya", dob: 1991, gender: "f"}
  |> Enum.filter(fn({_k, v}) -> is_binary(v) end)
  |> Enum.into(%{})
  |> IO.inspect(label: "back to map")


  IO.puts "take a number of elementd from an enumerable"
  IO.inspect Enum.take 1..10, 5


  IO.puts "Capture Operator: &"
  IO.inspect Enum.filter([1, 2, 3], &is_number/1)
  IO.inspect Enum.filter([1, "2", 3], &is_number(&1))
  IO.inspect Enum.reduce([1, 2, 3], 0, &(&1 + &2))
  IO.inspect Enum.reduce([1, 2, 3], &+/2)
  IO.inspect Enum.sum([1, 2, 4])
end
