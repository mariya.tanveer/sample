defmodule Scripts do
  def guess do
    IO.puts "Guess the Number between 1 to 10\n"
    guessed_num = IO.gets("try your luck!\n")
    {guessed_num, _str} = Integer.parse(guessed_num)

    cond do
      guessed_num == 6 ->
        IO.puts "Correct!"

      guessed_num > 6 ->
        IO.puts "Too high!"
        guess()

      guessed_num < 6 ->
         IO.puts "Too Low!"
         guess()

      true ->
        IO.puts "try again!"
    end

    case guessed_num do
      6 ->
        IO.puts "Correct!"

      _num ->
        # IO.inspect(num, label: "you entered")
        IO.puts "Wrong!"
        guess()
    end
    
    defstruct [:first_name, :last_name, :birthday :location]
  end

  def another_func do
    guess()
  end
end


# Scripts.guess()

{{:., [], [{:__aliases__, [alias: false], [:IO]}, :puts]}, [],
 ["This file was generated from Elixir"]}
