defmodule Search do
  @users [
          %{fname: "Sam", lname: "Johnson", cid: [10, 5]},
          %{fname: "Sam", lname: "Davis", cid: [5]},
          %{fname: "Sam", lname: "Garcia", cid: [3, 6, 11]},
          %{fname: "Sam", lname: "Miller", cid: [3, 6, 11]},
          %{fname: "Sam", lname: "Miller", cid: [10, 2]},
          %{fname: "Ali", lname: "Johnson", cid: [2]},
          %{fname: "Ali", lname: "Johnson", cid: [5, 10]}
  ]

  def search(search_query, %{cid: u_cid} = user) do
    @users
    |> Enum.reduce([], fn %{fname: fname, lname: lname, cid: cid}, acc ->
      if fname == search_query || lname == search_query do
        matchind_cids = Enum.reduce(cid, [], fn id, ac ->
          if id in u_cid, do: [id] ++ ac, else: ac
        end)

        IO.inspect(matchind_cids, label: "matchind_cids")

        [%{
          user: %{fname: fname, lname: lname, cid: cid},
          matchind_cids: matchind_cids
        }]
        ++ acc
      else
        acc
      end
    end)
  end
end

IO.inspect Search.search("Sam", %{fname: "Sam", lname: "Khan", cid: [10, 5]})
