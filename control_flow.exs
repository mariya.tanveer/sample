defmodule ControlFlow do

  # run the first block where the expression is truthy
  # cond do
    # expression -> code
    # true -> default
  # end
  def cond_flow do
    cond do
      1 + 1 == 1 ->
        "This will never happen"

      2 * 2 != 4 ->
        "Nor this"

      true ->
        "This will"
    end
  end

#  def case_macro do
#    case Stripe.Customer.create(attrs) do
#      {:ok, customer} ->
#        "A customer was created with ID #{customer.id}"
#
#      {:error, reason} ->
#        "Customer could not be created because of #{reason}"
#
#      other ->
#        "Unknown error occurred #{other}"
#    end
#  end

  @doc """
  if expr do
    code
  end

  use cond do end for elseif
  """
  def if_statement do
    if 1 + 1 == 3 do
      "This will never happen"
    else
      "This will happen"
    end
  end

  @doc """
  unless means except if
  unless expr do
    code
  end
  """
  def unless_statement do
    unless 1 + 1 == 3 do
      "The laws of Math still hold true"
    end
  end

  # pattern matching
  @doc """
    def blank?(value) do
      # Determine whether value is nil, false or ""

      # using case statement
      case value do
        nil -> true
        false -> true
        "" -> true
        _other -> false
      end
    end

  #pattern matching function
  """
#  def blank?(nil), do: true
#  def blank?(false), do: true
#  def blank?(""), do: true
#  def blank?(_other), do: false

  # Guards
#  def blank?(value) when value in [nil, false, ""], do: true
#  def blank?(_other), do: false

  # best solution
  def blank?(value), do: value in [nil, false, ""]

  # case statements also support guards
  def blank?(value) do
    case value do
      value when value in [nil, false, ""] -> true
      _ -> false
    end

    response = %{}
    case response do
      {:ok, body} ->
        "Success"

      {:error, status_code, body} when status_code in 400..499 ->
        "handle 400 status codes"

      {:error, status_code, body} when status_code in 500..599 ->
        "handle 500 status codes"

      _other ->
        "default"
    end
  end

  @doc """
  Static Typing
  Combined, pattern matching and guards can act like static typing:
  """
  def zero?(0) do
    true
  end

  def zero?(n) when is_number(n) do
    false
  end

  # Static Types
  def name(%User{} = user) do
    user.first_name <> " " <> user.last_name
  end

  def name(%Episode{name: name}) when is_binary(name) do
    name
  end

  def name(unsupported) do
    raise "name? does not support #{inspect unsupported}"
  end
end
