defmodule User do
  @moduledoc """
  Defines a User struct and functions to handle users.
  """

  import String, only: [split: 1]
  import List, only: [first: 1, last: 1]

  defstruct name: nil, email: nil

  @doc """
  Get the first name of a user.

  ## Parameters
  - `user` - A User struct.

  ## Example
      user = %User{name: "Daniel Smith"}
      User.first_name(user)
      "Daniel"
  """
  def first_name(user) do
    user
    |> get_names
    |> first
  end

  @doc """
  Get the last name of a user.

  ## Parameters
  - `user` - A User struct.

  ## Example
      user = %User{name: "Daniel Smith"}
      User.last_name(user)
      "Smith"
  """
  def last_name(user) do
    user
    |> get_names
    |> last
  end

  defp get_names(user) do
    split(user.name)
  end
end