defmodule DataType do

  def tuple do
    tuple = {"Daniel", 24}

    IO.inspect elem(tuple, 0)

    IO.inspect("--------------")

    IO.inspect elem(tuple, 1)

    # update existing value in tuple
    IO.inspect put_elem(tuple, 1, 25)
  end

  def list do
    ages = [12, 22, 34, 45]
    names = ["Ali", "Huma", "Raza", "Amna"]

    IO.inspect Enum.at(names, 2)

    list1 = [1, 2, 3]

    # prepending to a list is FAST
    IO.inspect list2 = [0 | list1], label: "prepend"

    # Appending to a list is SLOW
    IO.inspect list3 = list1 ++ [4], label: "append"

    # inserting elements can be SLOW
    list4 = [1, 3, 4]
    IO.inspect list5 = List.insert_at(list4, 1, 2), label: "insert at index 1"
  end

  def type_check do
    # is_type(value)

    IO.inspect is_atom(:all), label: "is_atom"
    IO.inspect is_list([2, 3]), label: "is_list"
    IO.inspect is_integer(2), label: "is_integer"

    IO.inspect is_map(2), label: "is_map"
    IO.inspect is_map(%{key: "value"}), label: "is_map"
  end

  # High Level Types
  # Keyword list [{:atom, value}]
  def keyword_list do
    attrs = [name: "Mariya Tanveer", email: "mariya.tanveer@invozone.com"]

    IO.inspect attrs[:name], label: "name"
    IO.inspect attrs[:email], label: "email"
  end

  # Struct %{__struct__: ModuleName, ...}
  def struct do
#     IO.inspect %Episode{
#      title: "Data Types",
#      author: "Daniel Berkompas"
#    }
  end

  # Ranges %Range{first: number, last: number}
  def range do
    IO.inspect 0..100, label: "0..100"
  end

  # Regular expressions %Regex{opts: ..., re_pattern: ...}
  def regular_expression do
    IO.inspect ~r/hello/, label: "~r/hello/ lab"
  end

  # more high level data types:
  # Tasks,
  # Agents,
  # Streams,
  # HashDicts,
  # HashSets
end