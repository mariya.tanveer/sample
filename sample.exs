def download_export(conn, opts) do
  model = opts[:model]
  records = opts[:records]
  available_fields = opts[:available_fields] || model.__schema__(:fields)
  
  model_basename_string = model 
    |> to_string 
    |> String.split(".") 
    |> Enum.at(-1)
  
  filename = "#{model_basename_string} #{elem(Timex.format(DateTime.utc_now, "{YYYY}-{0M}-{0D}-at-{h24}-{m}-{Zabbr}"), 1)}.csv"
  
  body_rows =
    records
    |> Enum.map(fn record ->
      Enum.map(available_fields, fn field -> Map.get(record, field) end)
    end)
    
  csv_stream =
    CSV.encode([available_fields | body_rows], separator: ?\,, delimiter: "\n")
  
  conn =
    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=\"#{filename}\"; charset=utf-8")
    |> send_chunked(200)
  
  csv_stream
    |> Enum.with_index
    |> Enum.each(fn {content, index} ->
      conn |> chunk(content)
    end)
  conn
end

def download_export(conn, params) do
  MESHTAUWeb.VersaCRUDControllerFunctions.download_export(
    conn,
    model: MESHTAU.Transaction,
    records: Repo.all(Ecto.Query.order_by(MESHTAUWeb.Admin.TransactionView.IndexView.base_query(conn.assigns), [desc: :insert_order])),
    available_fields: ~w(uuid inserted_at amount currency completed_merchant_order_uuid from_load_pack_code from_user_id from_merchant_uuid from_iso_uuid from_backing_bank_account_uuid from_mesh to_user_id to_merchant_uuid to_iso_uuid to_backing_bank_account_uuid to_mesh to_external_bank_account_uuid integrity_checksum)a
  )
end