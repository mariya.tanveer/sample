defmodule Operator do

  # Match operator =
  # pattern = data

  # variable names can be rebound
  name = "Daniel"
  name = "Ash"

   # complex multipart pattens can be used
  {name, age} = {"Daniel", 25}

  # use _ to ignore values
  {name, _} = {"Daniel", 25}
  {name, _age} = {"Ash", 30}

  # use variable values in match patterns with ^
  name = "Daniel"
  {^name, age} = {"Daniel", 25}

  # List operators
  def list_operators do
    IO.inspect "Daniel" in ["Daniel", "Ash", "Amir"], label: "in operator for list"
    IO.inspect "Daniel" in ["Duck", "Ash", "Amir"], label: "in operator for list"

    # combine two list with ++
    IO.inspect [1, 2, 3] ++ [4, 5], label: "combine two list with ++"

    # remove members from list with --
    IO.inspect [1, 2, 3, 4, 5] -- [1, 2, 4, 5], label: "remove members from list with --"

    # combine with | operator for complex matches
    list = [1, 2]
    IO.inspect [0 | list]
    [head | tail] = [1, 2, 3, 4, 5]
    [a, b, c | tail] = [1, 2, 3, 4]
  end

  def binary_operators do
    # Binary Operators <>
    # concatenate two binaries with <>
    IO.inspect "Hello" <> " " <> "World!"

    #interpolate values into binaries with #{}
    IO.inspect "the value of one plus two is: #{1 + 2}"

    name = "Daniel"
    IO.inspect "Hello, #{name}"

    # Compare binary to a pattern with =~
    IO.inspect "GoodBye" =~ ~r/Good/
    IO.inspect "GoodBye" =~ "Good"
    IO.inspect "Hello" =~ "World"
  end

  # Bitwise Operators

  # Logical Operators
  # assert two expressions are truthy with and, &&
  1 == 1 && 2 == 3 # => false

  name = "Daniel"
  age = 24
  name == "Daniel" and age > 23 # => true

  # return the first truthy expression with or, ||
  "Daniel" || nil # => "Daniel"
  nil || "Daniel" # => "Daniel"
  nil || false # => false

  user = %{name: nil, age: 26}
  name = user.name || "John Smith"

  # Pipeline operator
  foo = fn _ -> "foo" end
  bar = fn _ -> "bar" end

  var = 2
  var = foo.(var)
  var = bar.(var)

  # becomes this:
  var =
    var
    |> foo.()
    |> bar.()

  # which translates to:
  var = bar.(foo.(var))
end