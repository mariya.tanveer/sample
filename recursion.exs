defmodule Recursion do
  def length(list) do
    length(list, 0)
  end

  defp length([], count) do
    count
  end

  defp length([_|t], count) do
    length(t, count + 1)
  end

  def each([], _fun) do
    :ok
  end

  def each([h | t], fun) do
    fun.(h)
    each(t, fun)
  end

  # Not a tail call function
  # def join([h | t]) do
  #   h <> " " <> join(t)
  # end

  # tail call optimised
  def join([h | t], string) do
    string = string <> " " <> h
    join(t, string)
  end

  def join([], string) do
    string
  end

  def map(list, fun) do
    do_map(list, fun, [])
  end

  defp do_map([], _fun, acc) do
    :lists.reverse(acc)
  end

  defp do_map([h | t], fun, acc) do
    result = fun.(h)
    acc = [result | acc]
    do_map(t, fun, acc)
  end

  def sum(list) do
    do_sum(list, 0)
  end

  defp do_sum([h | t], acc) do
    do_sum(t, acc + h)
  end

  defp do_sum([], acc) do
    acc
  end

  def g_sum(list, func) do
    g_do_sum(list, func, 0)
  end

  defp g_do_sum([], _func, acc) do
    acc
  end

  defp g_do_sum([h | t], func, acc) do
    result = func.(h)
    g_do_sum(t, func, result + acc)
  end
end

Recursion.each([1, 2, 3], fn (num) ->
  IO.puts to_string(num)
#  IO.puts num
end)

IO.inspect Recursion.length([1, 3]), label: "length"

# Tail call optimisation, function call itself in last
IO.inspect Recursion.join(["a", "b", "c"], ""), label: "Recursion.join/2"

list = [
  {"Daniel", 24},
  {"Ash", 23}
]

IO.inspect Recursion.map(list, fn ({name, _age}) -> name end)

IO.inspect Recursion.sum([1, 2, 34]), label: "Sum"

IO.inspect Recursion.g_sum([1, 2, 34], fn (n) -> n * n end), label: "Generalised Sum"
