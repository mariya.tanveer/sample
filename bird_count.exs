defmodule BirdCount do
  def busy_days(list) do
    length(list, 0)
  end
  defp length([h|t], count)  when h >= 5 do
     length(t, count + 1)
  end
  defp length([h|t], count)  when h < 5 do
     length(t, count)
  end
  defp length([], count) do
    count
  end
end

IO.inspect BirdCount.busy_days([1, 5, 0])
